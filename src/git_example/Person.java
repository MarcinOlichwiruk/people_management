package git_example;

public class Person {
	
	private String name, surname;
	private int age;
	
	public Person(String name, String surname){
		this.name = name;
		this.surname = surname;
	}
	public Person(String name, String surname, int age){
		this(name, surname);
		this.setAge(age);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		if (age > 0)
			this.age = age;
	}
	//komentarz
	
	@Override
	public String toString(){
		return name+" "+surname+" "+age;
	}

}
