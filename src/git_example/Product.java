package git_example;

public class Product {
	
	private String name, description;
	
	public Product(String name){
		this.name = name;
	}

	public Product(String name, String description) {
		this.name = name;
		this.description = description;
	}


	@Override
	public String toString(){
		return name+" "+description;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
